import pandas as pd
import numpy as np
import datetime
from datetime import timedelta
from datetime import datetime
import time

#fetching raw file from the local PC.
xls = pd.ExcelFile(r'C:\Users\us\Desktop\Projects\Spotimyze Mobility\Parkonet\Reservation_Sales_report\Parkonect_Transaction_Data.xlsx')
df18 = pd.read_excel(xls, 'Jan 2018 - Dec 2018')
dfnew = df18.iloc[np.flatnonzero((df18=='Reservation ID').values)//df18.shape[1],:]
idx=dfnew.index
parkonect = pd.read_excel(xls, 'Jan 2018 - Dec 2018', header = idx+1)

parkonect = parkonect[:-1] # drop last n rows

# Name and Email fields contain NAN values. Therefore, replacing those with unknown as those fields are not retrieved from source.
parkonect['Name'] = parkonect['Name'].replace(np.nan, 'Unknown', regex=True)
parkonect['Email'] = parkonect['Email'].replace(np.nan, 'Unknown', regex=True)

parkonect = parkonect.sort_values('Start Date',ascending=True) #sorting values based on datetime

#created a dynamic function that creates a dataframe with required data for further analysis
def timeDiff(new_df ):
    edf = pd.DataFrame(columns=['id','Reservation_ID', 'Garage_ID','Lot_ID','Order_Date','Start_Date','End_Date','Reserve_Ahead_Time','Duration_Of_Parking','Vendor','Price','Fee','Net_Remit'])
    new_df['Order Date'] = pd.to_datetime(new_df['Order Date'])
    new_df['Start Date'] = pd.to_datetime(new_df['Start Date'])
    new_df['End Date'] = pd.to_datetime(new_df['End Date'])        
    Duration_Of_Parking = []
    Reserve_Ahead_Time = []
    for j in range(new_df.shape[0]):
            Reservation_ID = new_df['Reservation ID'].iloc[j]
            Order_Date = new_df['Order Date'].iloc[j]
            Start_Date = new_df['Start Date'].iloc[j]
            End_Date = new_df['End Date'].iloc[j]
            dateTimeDifference = pd.to_timedelta(new_df['End Date'].iloc[j] - new_df['Start Date'].iloc[j])/3600 
            dateTimeDifference2 = pd.to_timedelta(new_df['Start Date'].iloc[j] - new_df['Order Date'].iloc[j])/3600             
            Duration_Of_Parking = dateTimeDifference.total_seconds()
            Reserve_Ahead_Time =  dateTimeDifference2.total_seconds()
            Vendor = new_df.Vendor.iloc[j]
            Price = new_df.Price.iloc[j]
            Fee = new_df.Fee.iloc[j]
            Lot = '49'
            Garage_ID = '2'
            Net_Remit = new_df['Net Remit'].iloc[j]
            edf = edf.append({'Reservation_ID':Reservation_ID,'Garage_ID':Garage_ID,'Lot_ID': Lot,'Order_Date': Order_Date,'Start_Date':Start_Date,'End_Date': End_Date,'Duration_Of_Parking': Duration_Of_Parking,'Reserve_Ahead_Time':Reserve_Ahead_Time,'Vendor':Vendor,'Price':Price,'Fee':Fee,'Net_Remit':Net_Remit},ignore_index=True)
    
    #You can mention any path in your PC here. This will save the clean output file.
    outpath = r'D:\Parkonect_Rervation_Sales_Report'
    
    #saving the filename as per datetime. Whenever the file will run the current time willbe appended to the filename.
    filename = "Clean_PARKCONECT_Reservation_Sales_Rpt_"+(datetime.now().strftime("%m%d%Y_%H%M"))+".csv"
    edf.to_csv(outpath + "\\" + filename, index = False)
    return edf

edf = timeDiff(parkonect)